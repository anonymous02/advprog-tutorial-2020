package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.springframework.stereotype.Service;

@Service
public class HolyGrail {
    // TODO complete me

    private HolyWish holyWish;

    public void makeAWish(String wish) {
        // TODO complete me

        this.holyWish = this.getHolyWish();
        this.holyWish.setWish(wish);
    }

    public HolyWish getHolyWish() {
        // TODO fix me

        return HolyWish.getInstance();
    }
}
