package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    private AcademyRepository academyRepository;
    private AcademyService academyService;

    // TODO create tests

    @BeforeEach
    public void setUp() {
        this.academyRepository = new AcademyRepository();
        this.academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testGetAcademies() {
        assertEquals(2, academyService.getKnightAcademies().size());
    }

    @Test
    public void testProduceKnight() {
        this.academyService.produceKnight("Lordran", "majestic");
        assertEquals("Majestic Knight", this.academyService.getKnight().getName());
    }
}
