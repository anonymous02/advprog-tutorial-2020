package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = mock(HolyGrail.class);
    }

    @Test
    public void testWishShouldExist() {
        holyGrail.makeAWish("Ganti PA");
        verify(holyGrail, atLeast(1)).makeAWish("Ganti PA");
    }

    @Test
    public void testGetHolyWish() {
        doCallRealMethod().when(holyGrail).getHolyWish();
        HolyWish holyWish = holyGrail.getHolyWish();
        assertNotNull(holyWish);
    }
}
