package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me

        this.drangleicAcademy = new DrangleicAcademy();
        System.out.println();
        this.majesticKnight = drangleicAcademy.getKnight("majestic");
        this.metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        this.syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test

        assertTrue(this.majesticKnight.getArmor() != null && this.majesticKnight.getWeapon() != null);
        assertTrue(this.metalClusterKnight.getArmor() != null && this.metalClusterKnight.getSkill() != null);
        assertTrue(this.syntheticKnight.getWeapon() != null && this.syntheticKnight.getSkill() != null);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test

        assertEquals("Majestic Knight", this.majesticKnight.getName());
        assertEquals("Metal Cluster Knight", this.metalClusterKnight.getName());
        assertEquals("Synthetic Knight", this.syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test

        assertEquals("Majestic Knight", this.majesticKnight.getName());
        assertEquals("Metal Cluster Knight", this.metalClusterKnight.getName());
        assertEquals("Synthetic Knight", this.syntheticKnight.getName());
    }

}
