package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me

                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me

        public void update() {
                if(this.guild.getQuestType().equals("D") || this.guild.getQuestType().equals("R") || this.guild.getQuestType().equals("E")) {
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
