package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;

import java.util.List;
import java.util.Optional;

// TODO: Import service bean
@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service

    private List<Soul> souls;

    @Autowired
    private final SoulRepository soulRepository;

    public SoulServiceImpl(SoulRepository soulRepository) {
        this.soulRepository = soulRepository;
    }

    @Override
    public List<Soul> findAll() {
        return soulRepository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return this.soulRepository.findById(id);
    }

    @Override
    public void erase(Long id) {
        this.soulRepository.deleteById(id);
    }

    @Override
    public Soul register(Soul soul) {
        return this.soulRepository.save(soul);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return this.soulRepository.save(soul);
    }
}
