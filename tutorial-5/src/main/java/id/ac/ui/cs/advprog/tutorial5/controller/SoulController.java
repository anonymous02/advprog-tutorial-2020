package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

// TODO: Import apapun yang anda perlukan agar controller ini berjalan

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;

    public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        // TODO: Use service to complete me.

        return new ResponseEntity<>(soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        // TODO: Use service to complete me.

        return new ResponseEntity(soulService.register(soul), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // TODO: Use service to complete me.

        Optional<Soul> soul = soulService.findSoul(id);

        if(soul.isPresent()) {
            return new ResponseEntity<>(soul.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.

        return new ResponseEntity<>(soulService.rewrite(soul), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        // TODO: Use service to complete me.

        soulService.erase(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
