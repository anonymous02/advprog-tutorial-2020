package id.ac.ui.cs.advprog.tutorial5.service;

import static org.mockito.Mockito.lenient;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    private Soul soul;

    @InjectMocks
    SoulServiceImpl soulService;

    @BeforeEach
    public void setUp() {
        this.soul = new Soul();
        this.soul.setId(11);
        this.soul.setName("A");
        this.soul.setAge(20);
        this.soul.setGender("M");
        this.soul.setOccupation("Student");
        this.soulService.register(this.soul);
    }

    @Test
    public void testFindAll() {
        List<Soul> soulList = this.soulRepository.findAll();
        lenient().when(soulService.findAll()).thenReturn(soulList);
    }

    @Test
    public void testFindSoul() {
        long idExist = 11;
        long idNotExist = 12;
        Optional<Soul> soulExist = this.soulRepository.findById(idExist);
        lenient().when(soulService.findSoul(idExist)).thenReturn(soulExist);
        lenient().when(soulService.findSoul(idNotExist)).thenReturn(null);
    }

    @Test
    public void testEraseSoul() {
        long id = 11;
        soulService.erase(id);
        lenient().when(soulService.findSoul(id)).thenReturn(Optional.empty());
    }

    @Test
    public void testRewriteSoul() {
        lenient().when(soulService.rewrite(this.soul)).thenReturn(this.soul);
        soulService.rewrite(this.soul);
    }

    @Test
    public void testRegisterSoul() {
        Soul newSoul = new Soul();
        lenient().when(soulService.register(newSoul)).thenReturn(newSoul);
    }
}