package id.ac.ui.cs.advprog.tutorial5.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/soul"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    Soul souls;

    @BeforeEach
    public void setUp() {
        souls = new Soul();
        souls.setId(1);
        souls.setName("A");
        souls.setAge(20);
        souls.setGender("M");
        souls.setOccupation("Student");
    }

    @Test
    public void testCreate() throws Exception {
        Soul soul = new Soul();
        soul.setId(11);
        soul.setName("A");
        soul.setAge(20);
        soul.setGender("M");
        soul.setOccupation("Student");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String requestBody = objectWriter.writeValueAsString(soul);

        mockMvc.perform(post("/soul")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindByIdNotExist() throws Exception {
        mockMvc.perform(get("/soul/12"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateSoul() throws Exception {
        Soul soul = new Soul();
        soul.setId(11);
        soul.setName("A");
        soul.setAge(20);
        soul.setGender("M");
        soul.setOccupation("Student");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestBody = writer.writeValueAsString(soul);

        mockMvc.perform(put("/soul/11")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteSoul() throws Exception {
        mockMvc.perform(delete("soul/1"))
                .andExpect(status().isNotFound());
    }
}
