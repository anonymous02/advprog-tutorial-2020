package id.ac.ui.cs.advprog.tutorial5.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class SoulTest {

    Soul soul;

    @BeforeEach
    public void setUp() {
        this.soul = new Soul();
        this.soul.setId(11);
        this.soul.setName("A");
        this.soul.setAge(20);
        this.soul.setGender("M");
        this.soul.setOccupation("Student");
    }

    @Test
    public void testGetID() {
        assertEquals(11, this.soul.getId());
    }

    @Test
    public void testGetName() {
        assertEquals("A", this.soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(20, this.soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals("M", this.soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Student", this.soul.getOccupation());
    }
}